---
title: "Tasks"
author: "Antti Heliste"
date: "7 August 2018"
output: 
  pdf_document:
    toc: true
    number_sections: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(readr)
library(dplyr)
```

#Market risk

##RM1

**1. Let us assume that we have an initial position in EUR/DKK of 64.000 EUR and the current FX-Price = 750.25. We need to measure our risk assuming a 10-day unwind period and as our risk-management model is based on a normal assumption. we just need to determine the volatility which we estimate to be equal to: 0.09% p.a. The selected confidence level is 99%.**

*What is VaR*

The volatility is given per annum so we have to calculate the daily volatility:

$\sigma = 0.09/\sqrt{252} \cdot 64 \cdot 7.5025 = 2.722$.

$VaR = \sigma N^{-1} (0.99)\sqrt{10} = 2.722 \cdot 2.33 \cdot \sqrt{10} = 20.0$.

*What is CVaR*

$CVaR = \sigma\sqrt{10}\frac{e^{-Y^2/2}}{\sqrt{2\pi}(1 - X)}=2.722\sqrt{10}\frac{e^{-2.33^2/2}}{\sqrt{2\pi}(1 - 0.99)}=22.9$

*Which one is highest VaR or CVaR? And why?*

CVar is higher because it is the average of the tail whereas VaR is just the first point of the tail.

##RM2

**1. Explain the difference between VaR and CvaR?**

VaR is the limit that we will not lose with X confidence.

CVaR is the expected loss if (1-X) case occurs. It is the average of the tail.

**2. A company has entered into a forward contract to buy 1 million DKK for 200.000 USD. The contract has a 6-month to maturity. The daily volatilities of a 6-month zero-coupon Danish Bond (when its price is translated to dollars) is 0.06% and the daily volatility of a 6-month zero-coupon Dollar Bond is 0.05%. The correlation between return is 0.7. The current exchange rate is 5.2. (Assume that the 6-month Danish interest rate is 3% and the 6-month dollar interest rate is 1.5%) **

*Calculate the standard deviation of the change in dollar value of the forward contract in 1-day?*

We can solve the result using the following formula: 

$$\sigma_p^2 = \alpha^TC\alpha$$

where $\alpha$ is a vector of the cash amounts and $C$ is the variance-covariance matrix.

```{r }


cor_matrix <- matrix(c(1, 0.7, 0.7, 1), nrow = 2)
corr <- 0.7

dkk_i <- 0.03
dkk_sd <- 0.0006

usd_i <- 0.015
usd_sd <- 0.0005

spot <- 5.2
T <- 1
hor <- 10

usd <- 200000
dkk <- 1000000

#Sd
varco_matrix <-
matrix(c(dkk_sd ^ 2, corr * dkk_sd * usd_sd, corr * dkk_sd * usd_sd, usd_sd ^
2),
nrow = 2)

rmatrix <- matrix(c(dkk * exp(-dkk_i * T) / spot, usd * exp(-usd_i * T)))

sd_tot <- as.numeric(sqrt(t(rmatrix) %*% (varco_matrix %*% rmatrix)))


```

The standard deviation equals `r toString(round(sd_tot, 2))`.

*What is the 10-day 99% VaR?*

```{r }
#10 day car

var_10 <- as.numeric(sd_tot * qnorm(0.99) * sqrt(10))

```

The 10-day VaR is `r toString(round(var_10, 2))`

**3. Explain why the linear model can only provide approximate estimates of VaR for a portfolio containing options**

Because there might non-linear correlation between the portfolio investments. Also their return might not be normally distributed.

**4: A company uses an EWMA model for forecasting volatility. It decides to change the parameter lambda from 0.95 to 0.84. Explain the likely impact on the forecasts.**

Recent observations will have a larger impact on the volatility.

**5: A company uses the Garch(1,1) model for updating volatility. The 3 parameters are omega, alpha and beta. Describe the impact of making a small increase in each of the parameters while keeping the others fixed.**

* Omega - The impact of long term variance increases.
* Alpha - The impact of the most recent observation increases.
* Beta - The impact of the most recent volatility observation increases.

**6: Let us assume that SP500 at close of trading yesterday was 1.040 and the daily volatility of the index was estimated as 1% per day at that time. The parameters in a Garch(1,1) model are:**

* Omega = 0.000002
* Alpha = 0.06
* Beta = 0.92

*If the level of the index at close of trading today is 1.060 what is the new volatility estimate?*

Using the formula:

$$ \sigma_n^2 = \omega + \alpha u_{n-1}^2 + \beta \sigma_{n-1}^2 $$

where $u=(1060-1040)/1040$ = `r signif((1060-1040)/1040,3)`.

Thus:

$\sigma_n^2 = 0.000002 + 0.06 \cdot 0.0192^2 + 0.92 \cdot 0.01^2$ = `r toString(signif(0.000002 + 0.06*0.0192^2 + 0.92*0.01^2,3))`.

and

$\sigma_n$ = `r signif(sqrt(0.000002 + 0.06*0.0192^2 + 0.92*0.01^2),3)`.

The original volatility was:

$V_{n-1}= \sqrt{\omega / (1-\alpha - \beta)} = \sqrt{0.000002 / (1- 0.92 - 0.06)}$ = `r signif(sqrt(0.000002/(1-0.92-0.06)),3)`

The volatility increased.

**7: The parameters of a Garch(1,1) model are estimated as omega = 0.000004, alpha = 0.05, beta = 0.92.**

*What is the long-run average volatility and what is the equation describing the way that the variance rate reverts to its long-run average?*

$\sigma_L = \sqrt{\omega / (1-\alpha - \beta)} = \sqrt{0.000004 / (1- 0.05 - 0.92)}$ = `r signif(sqrt(0.000004/(1-0.05-0.92)),3)`

If current variance rate is higher $V_L$ than the variance will decrease and vice versa.

*If the current volatility is 20% per year, what is the expected volatility in 20-days?*

The daily volatility is $\sigma_d = 0.2/\sqrt{252}$ = `r 0.2/sqrt(252)`.

The expeced volatility is:

$E[\sigma_{n+20}]= \sqrt{V_L+(\alpha+\beta)^t (\sigma_n^2-V_L)}=\sqrt{0.00013+(0.05+0.92)^{20} (0.0115^2-0.00013)}$ = `r toString(signif(sqrt(0.000004/(1-0.05-0.92) + (0.05+0.92)^20*((0.2/sqrt(252))^2 - 0.000004/(1-0.05-0.92))),3))`

The daily (and yearly) volatility will unsurprsingly decrease.

##RM3

*Calculate VaR under the assumption that the series are normal distributed. For h = 1-day and at the quantile 99% and 99.5%, assume a zero mean*

```{r cache = T}

SPnas <- read_csv("Book1.csv")
SPnas <- SPnas %>% select("SP500", "NASDAQ") / 100

varco_matrix <- cov(SPnas)

invest <- matrix(c(2500000, 2500000))

SPnas_sd <- sqrt(t(invest) %*% varco_matrix %*% invest)

SPnas_99 <- SPnas_sd * qnorm(0.99)
SPnas_995 <- SPnas_sd * qnorm(0.995)

```

The 99% VaR is `r toString(signif(SPnas_99/5000000,3))`%.

The 99.5% VaR is `r toString(signif(SPnas_995/5000000,3))`%.

*Calculate CVaR under the assumption that the series are normal distributed. For h = 1-day and at the quantile 99% and 99.5%, assume a zero mean
What now if the data series is not normal distributed?*

```{r }
#CvaR

SPnas_c99 <-
  SPnas_sd * exp(-1 * qnorm(0.99) ^ 2 / 2) / (sqrt(2 * pi) * (1 - 0.99))
  
SPnas_c995 <- SPnas_sd * exp(-1 * qnorm(0.995) ^ 2 / 2) / (sqrt(2 * pi) * (1 - 0.995))

```

The 99% CVaR is `r toString(signif(SPnas_c99/5000000,3))`%.

The 99.5% CVaR is `r toString(signif(SPnas_c995/5000000,3))`%.

*If data is not normal, how can one calculate VaR and CVaR?*

Just need to use the real distribution

```{r }

SPnas_sum <- SPnas %>% 
  mutate(sum = 0.5*SP500 + 0.5*NASDAQ) %>% 
    arrange(sum) %>%
      select(sum) %>% 
        abs()

n <- nrow(SPnas_sum)

#VaR

SPnas_99d <- SPnas_sd * SPnas_sum[floor(n*0.01), ] * 100
SPnas_995d <- SPnas_sd * SPnas_sum[floor(n*0.005), ] * 100


```

The 99% VaR is `r toString(signif(SPnas_99d/5000000,3))`%.

The 99.5% VaR is `r toString(signif(SPnas_995d/5000000,3))`%.

```{r}

SPnas_c99d <- SPnas_sd * mean(SPnas_sum[1:floor(n*0.01), ]) * 100


SPnas_c995d <- SPnas_sd * mean(SPnas_sum[1:floor(n*0.005), ]) * 100

```

The 99% CVaR is `r toString(signif(SPnas_c99d/5000000,3))`%.

The 99.5% CVaR is `r toString(signif(SPnas_c995d/5000000,3))`%.


